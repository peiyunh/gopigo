from pybrain.rl.environments import EpisodicTask
from .environment import NavigationEnvironment
import time

def get_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

def send(sock, obj):
    print get_time(), 'NavigationTask send()'
    msg = pickle.dumps(obj)
    sock.sendall(msg)

def recv(sock):
    print get_time(), 'NavigationTask recv()' 
    msg = sock.recv(1024)
    obj = pickle.loads(msg)
    return obj

class NavigationTask(EpisodicTask):
    """
    env: an instance of a NavigationEnvironment
    maxsteps: maximal number of steps (default: 1000)
    """
    def __init__(self, env=None, maxsteps=1000):
        print get_time(), 'initializing NavigationTask'
        if env == None:
            env = NavigationEnvironment()
        EpisodicTask.__init__(self, env)
        self.N = maxsteps
        self.t = 0

        self.sensor_limits = [200]*13 + [480,360]
        #self.actor_limits = [(-90,90), (-30,+30)]

        self.reward_scale = 100 / maxsteps / 480

    def reset(self):
        print get_time(), 'resetting NavigationTask'
        EpisodicTask.reset(self)
        self.t = 0

    def performAction(self, action):
        print get_time(), 'performing action of NavigationTask'
        self.t += 1
        EpisodicTask.performAction(self, action)

    def isFinished(self):
        print get_time(), 'testing if finished' 
        if self.t >= self.N:
            return True
        return False

    def getReward(self):
        print get_time(), 'waiting to receive the reward'
        size = recv(self.env.conn)
        return self.reward_scale * size

    def setMaxLength(self, n):
        print get_time(), 'setting the maximum length'
        self.N = n
