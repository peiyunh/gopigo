import os, sys, pickle, copy, time
import numpy as np
import socket
import cv2
import pdb
from collections import Counter
import math
from socketIO_client import SocketIO, LoggingNamespace

sys.path.append('GoPiGo/Software/Python')
#from gopigo import *

def get_time():
    return time.strftime("%Y-%m-%d %H:%M:%S", time.gmtime())

def init_socketIO():
    remote_ip = '127.0.0.1'
    remote_port = 15000
    socketIO = SocketIO(remote_ip, remote_port, LoggingNamespace)
    return socketIO

def init_camera():
    cam = cv2.VideoCapture(0)
    cam.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, 480)
    cam.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, 360)
    if not cam.isOpened:
        print 'Error in opening cameras'
        sys.exit(1)
    else:
        return cam

def init_object():
    roi_path = 'cache/roi.p'
    if not os.path.exists(roi_path):
        print 'Error in finding roi cache'
    else:
        roi = pickle.load(open(roi_path, 'rb'))
    roi_hsv = cv2.cvtColor(roi, cv2.COLOR_BGR2HSV)
    hsv_vec = roi_hsv.reshape(roi_hsv.shape[0]*roi_hsv.shape[1], roi_hsv.shape[2])
    # boundary for hue
    l_hsv = np.mean(hsv_vec,0) - 5*np.std(hsv_vec,0)
    h_hsv = np.mean(hsv_vec,0) + 5*np.std(hsv_vec,0)
    # boundary for saturation and value
    l_hsv[1:] = 30
    h_hsv[1:] = 255
    return (l_hsv, h_hsv)

center = [0, 0]
radius = 0
def recognize(im, lb, ub):
    global center, radius
    skern = np.ones((3,3), np.uint8)
    mkern = np.ones((5,5), np.uint8)
    bkern = np.ones((11,11), np.uint8)
    hsv = cv2.cvtColor(im, cv2.COLOR_BGR2HSV)
    mask = cv2.inRange(hsv, lb, ub)
    if np.count_nonzero(mask) <= 100:
        return []
    else:
        mask = cv2.dilate(mask, np.ones((3,3), np.uint8))
        mask = cv2.erode(mask, np.ones((5,5), np.uint8))
        mask = cv2.dilate(mask, np.ones((11,11), np.uint8))
        contours = copy.copy(mask)
        ctrs,src = cv2.findContours(contours,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
        ctr_areas = [cv2.contourArea(ctr) for ctr in ctrs]
        if len(ctr_areas) > 0:
            #cv2.imshow('Contours', contours)
            del ctrs[np.argmax(ctr_areas)]
            cv2.drawContours(mask,ctrs,-1,0,cv2.cv.CV_FILLED)
        # estimate the centroid and radius
        yy,xx = np.where(mask==255)
        ymean,xmean = np.mean(yy), np.mean(xx)
        radius = 1.414*np.mean(np.sqrt((yy-ymean)**2+(xx-xmean)**2))
        radius = np.uint(radius)
        center = [np.uint(xmean), np.uint(ymean)]

    center = [int(c.item()) for c in center]
    radius = int(np.asscalar(radius))
    #return center, radius

angs = [1,15,30,45,60,75,90,105,120,135,150,165,179]
dists = [0]*len(angs)
def us_scan():
    global angs, dists

    enable_servo()
    delay = 0.02
    incr = 15
    lim = 200
    num_sample = 3
    for i in range(len(angs)):
        # move the servo to the angle
        servo(angs[i])
        # read
        buf = [0]*num_sample
        for j in range(num_sample):
            buf[j] = us_dist(15)
        mask = np.logical_or(buf < 0, buf > lim)
        buf[mask] = lim
        counts = np.bincount(buf)
        dists[i] = np.argmax(counts)
        # wait for a while
        time.sleep(delay) # TODO: can we communicate instead of calling sleep?
    disable_servo()
    #return angs, dists

#def on_request(*args):
    #pdb.set_trace()
#    print get_time(), 'receive request', args
    #self.emit('sensor')

def on_command(data):
    print get_time(), 'executing received command', data
    #eval(data['action'])

# init the conenction
socketIO = init_socketIO()
#socketIO.on('request', on_request)
socketIO.on('command', on_command)

# let the car move around
cam = init_camera()
[lb,ub] = init_object()

#
iter = 0

#
while True:
    # read image
    #print get_time(), 'reading camera sensors'
    sf, im = cam.read()

    # read distance sensors
    #print get_time(), 'reading ultrasonic sensors'
    #angs, dists = us_scan()

    # recognize and read distance sensors
    recognize(im, lb, ub) # read center and radius from global context
    cv2.circle(im,tuple(center),radius,(0,255,0),4)
    cv2.imwrite('cache/snapshots/'+get_time()+'.jpg', im)

    # send over the sensor information (for now we do not use center information)
    print get_time(), 'sending sensor data over'
    socketIO.emit('sensor', {'dist':dists})
    socketIO.wait(seconds=1)

    # wait for action and perform the action
    socketIO.wait(seconds=1)

    # send over the reward
    sf, im = cam.read()
    recognize(im, lb, ub)
    socketIO.emit('reward', {'size': radius})
    socketIO.wait(seconds=1)

    time.sleep(0.2)
    iter += 1

# close the socket
sock.close()

# release the camera
cam.release()
